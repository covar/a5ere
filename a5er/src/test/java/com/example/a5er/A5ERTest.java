package com.example.a5er;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

import org.junit.Test;

import com.example.a5er.section.EntityField;
import com.example.a5er.section.ISectionField;
import com.example.a5er.section.RelationField;

/**
 * Unit test for A5ER.
 */
public class A5ERTest {

	@Test
	public void 読み込みデータと保存データが一致すること() throws IOException {
		URL url = this.getClass().getClassLoader().getResource("test001.a5er");
		File file = new File(url.getPath());
		String expected = Files.lines(file.toPath(), StandardCharsets.UTF_8)
				.collect(Collectors.joining(A5ER.LINE_DELIMITER));

		A5ER a5er = A5ER.load(url.getPath());

		StringWriter writer = new StringWriter();
		a5er.write(writer);
		String actual = writer.getBuffer().toString();
		assertThat(actual, is(expected));
	}

	@Test
	public void 指定したフィールドのみ保存されること() throws IOException {
		URL expectedUrl = this.getClass().getClassLoader().getResource("target_field_output.a5er");
		File expectedFile = new File(expectedUrl.getPath());
		String expected = Files.lines(expectedFile.toPath(), StandardCharsets.UTF_8)//
				.collect(Collectors.joining(A5ER.LINE_DELIMITER));

		URL url = this.getClass().getClassLoader().getResource("test001.a5er");
		A5ER a5er = A5ER.load(url.getPath());

		StringWriter writer = new StringWriter();
		Collection<ISectionField> fields = new HashSet<>();
		fields.addAll(Arrays.asList(EntityField.PAGE, EntityField.LEFT, EntityField.TOP, EntityField.POSITION,
				EntityField.Z_ORDER));
		fields.addAll(Arrays.asList(RelationField.POSITION, RelationField.BAR_1, RelationField.BAR_2,
				RelationField.BAR_3, RelationField.TERMPOS_1, RelationField.TERMPOS_2, RelationField.Z_ORDER));
		a5er.write(writer, fields);

		String actual = writer.getBuffer().toString();
		assertThat(actual, is(expected));
	}

	@Test
	public void 別ファイルからデータをマージできること() throws IOException {
		URL expectedUrl = this.getClass().getClassLoader().getResource("merge_result.a5er");
		File expectedFile = new File(expectedUrl.getPath());
		String expected = Files.lines(expectedFile.toPath(), StandardCharsets.UTF_8)//
				.collect(Collectors.joining(A5ER.LINE_DELIMITER));

		URL baseUrl = this.getClass().getClassLoader().getResource("test001.a5er");
		A5ER baseA5er = A5ER.load(baseUrl.getPath());

		URL srcUrl = this.getClass().getClassLoader().getResource("merge_src.a5er");
		A5ER mergeSrcA5er = A5ER.load(srcUrl.getPath());

		// マージ
		baseA5er.mergeManager(mergeSrcA5er);
		baseA5er.mergeEntity(mergeSrcA5er);
		baseA5er.mergeRelation(mergeSrcA5er);

		// 出力
		StringWriter writer = new StringWriter();
		baseA5er.write(writer);

		String actual = writer.getBuffer().toString();
		assertThat(actual, is(expected));
	}
}
