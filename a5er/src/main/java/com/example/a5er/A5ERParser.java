package com.example.a5er;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.example.a5er.section.AbstractSection;
import com.example.a5er.section.EntitySection;
import com.example.a5er.section.EntitySection.EntitySectionKey;
import com.example.a5er.section.ISectionField;
import com.example.a5er.section.ManagerSection;
import com.example.a5er.section.RelationSection;
import com.example.a5er.section.RelationSection.RelationSectionKey;
import com.example.a5er.section.SectionType;

public class A5ERParser {

	private A5ER a5er;

	A5ER parse(List<String> lines) {
		this.a5er = new A5ER();

		List<String> remaining = lines;
		// BOM除去
		if (CollectionUtils.isNotEmpty(remaining)) {
			String first = remaining.get(0);
			if (first.startsWith(A5ER.UTF8_BOM)) {
				first = first.substring(1);
			}
			remaining.set(0, first);
		}

		while (CollectionUtils.isNotEmpty(remaining)) {
			String line = remaining.get(0);
			// header
			if (line.startsWith(A5ER.HEADER_PREFIX)) {
				remaining = parseHeader(remaining);
				continue;
			}

			remaining = remaining.subList(1, remaining.size());
			SectionType section = SectionType.enumOf(line)
					.orElseThrow(() -> new RuntimeException("Section name is not defined: " + line));
			switch (section) {
			case MANAGER:
				remaining = parseManager(remaining);
				break;
			case ENTITY:
				remaining = parseEntity(remaining);
				break;
			case RELATION:
				remaining = parseRelation(remaining);
				break;
			}
		}

		return this.a5er;
	}

	private List<String> parseHeader(List<String> lines) {
		int i;
		for (i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			if (A5ER.SECTION_DELIMITER.equals(line)) {
				break;
			}

			a5er.addHeader(line);
		}

		int size = lines.size();
		if (i + 1 < size) {
			return lines.subList(i + 1, size);
		} else {
			return Collections.emptyList();
		}
	}

	private List<String> parseManager(List<String> lines) {
		ManagerSection section = new ManagerSection();
		List<String> remaining = parseSection(section, lines);

		a5er.setManagerSection(section);

		return remaining;
	}

	private List<String> parseEntity(List<String> lines) {
		EntitySection section = new EntitySection();
		List<String> remaining = parseSection(section, lines);

		EntitySectionKey key = section.generateKey();
		a5er.addEntity(key, section);

		return remaining;
	}

	private List<String> parseRelation(List<String> lines) {
		RelationSection section = new RelationSection();
		List<String> remaining = parseSection(section, lines);

		RelationSectionKey key = section.generateKey();
		a5er.addRelation(key, section);

		return remaining;
	}

	private <F extends ISectionField> List<String> parseSection(AbstractSection<F> section, List<String> lines) {
		int i;
		for (i = 0; i < lines.size(); i++) {
			String line = lines.get(i);
			if (A5ER.SECTION_DELIMITER.equals(line)) {
				break;
			}

			String[] fieldValues = line.split(A5ER.FIELD_VALUE_DELIMITER);
			String fieldName = fieldValues[0];
			String value;
			if (fieldValues.length == 1) {
				value = "";
			} else if (fieldValues.length == 2) {
				value = fieldValues[1];
			} else {
				value = Arrays.asList(fieldValues).subList(1, fieldValues.length).stream()//
						.collect(Collectors.joining(A5ER.FIELD_VALUE_DELIMITER));
			}

			section.addField(fieldName, value);
		}

		int size = lines.size();
		if (i + 1 < size) {
			return lines.subList(i + 1, size);
		} else {
			return Collections.emptyList();
		}

	}

}
