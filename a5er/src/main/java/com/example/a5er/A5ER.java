package com.example.a5er;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.example.a5er.section.AbstractSection;
import com.example.a5er.section.AbstractSection.ISectionKey;
import com.example.a5er.section.EntityField;
import com.example.a5er.section.EntitySection;
import com.example.a5er.section.EntitySection.EntitySectionKey;
import com.example.a5er.section.ISectionField;
import com.example.a5er.section.IValue;
import com.example.a5er.section.ManagerField;
import com.example.a5er.section.ManagerSection;
import com.example.a5er.section.RelationField;
import com.example.a5er.section.RelationSection;
import com.example.a5er.section.RelationSection.RelationSectionKey;
import com.example.a5er.util.ArgValidateUtil;

/**
 * A5ER
 * 
 * @author covar
 */
public class A5ER {

	public static final Charset CHARSET = StandardCharsets.UTF_8;

	public static final String UTF8_BOM = "\uFEFF";

	public static final String HEADER_PREFIX = "#";

	public static final String LINE_DELIMITER = "\r\n";

	public static final String SECTION_DELIMITER = "";

	public static final String FIELD_VALUE_DELIMITER = "=";

	private List<String> header = new ArrayList<>();

	private ManagerSection managerSection;

	private Map<EntitySectionKey, EntitySection> entityMap = new LinkedHashMap<>();

	private Map<RelationSectionKey, RelationSection> relationMap = new LinkedHashMap<>();

	A5ER() {
	}

	/**
	 * a5erファイルを読み込む。
	 * 
	 * @param filePath
	 * @return
	 */
	public static A5ER load(String filePath) {
		ArgValidateUtil.notNull(filePath);

		File file = new File(filePath);
		try (FileReader fr = new FileReader(file, CHARSET)) {
			return read(fr);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * a5erデータを読み込む。
	 * 
	 * @param reader
	 * @return
	 * @throws IOException
	 */
	public static A5ER read(Reader reader) throws IOException {
		List<String> lines = new ArrayList<>();

		BufferedReader br = new BufferedReader(reader);
		String line;
		while ((line = br.readLine()) != null) {
			lines.add(line);
		}

		A5ERParser parser = new A5ERParser();
		A5ER a5er = parser.parse(lines);

		return a5er;
	}

	/**
	 * a5erファイルを保存する。
	 * 
	 * @param filePath
	 */
	public void save(String filePath) {
		ArgValidateUtil.notNull(filePath);

		File file = new File(filePath);
		try (FileWriter fw = new FileWriter(file, CHARSET)) {
			write(fw);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 指定したフィールドの値を保存する。
	 * 
	 * @param filePath
	 * @param fields
	 */
	public void save(String filePath, Collection<ISectionField> fields) {
		ArgValidateUtil.notNull(filePath);

		File file = new File(filePath);
		try (FileWriter fw = new FileWriter(file)) {
			write(fw, fields);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * a5erデータを書き込む。
	 * 
	 * @param writer
	 * @throws IOException
	 */
	public void write(Writer writer) throws IOException {
		ArgValidateUtil.notNull(writer);

		Collection<ISectionField> fields = new HashSet<>();
		fields.addAll(Arrays.asList(ManagerField.values()));
		fields.addAll(Arrays.asList(EntityField.values()));
		fields.addAll(Arrays.asList(RelationField.values()));

		write(writer, fields);
	}

	/**
	 * 指定されたフィールドのみa5erデータを書き込む。
	 * 
	 * @param writer
	 * @param fields
	 * @throws IOException
	 */
	public void write(Writer writer, Collection<ISectionField> fields) throws IOException {
		ArgValidateUtil.notNull(writer, fields);

		writer.write(UTF8_BOM);

		// header
		for (String h : this.header) {
			writer.write(h + LINE_DELIMITER);
		}

		// Manager
		Collection<ManagerField> managerFields = fields.stream()//
				.filter(f -> f instanceof ManagerField)//
				.map(f -> (ManagerField) f)//
				.collect(Collectors.toSet());
		writer.write(SECTION_DELIMITER + LINE_DELIMITER);
		this.managerSection.write(writer, managerFields);

		// Entity
		Collection<EntityField> entityFields = fields.stream()//
				.filter(f -> f instanceof EntityField)//
				.map(f -> (EntityField) f)//
				.collect(Collectors.toSet());
		writer.write(SECTION_DELIMITER + LINE_DELIMITER);
		writeSections(writer, this.entityMap.values(), entityFields);

		// Relation
		Collection<RelationField> relationFields = fields.stream()//
				.filter(f -> f instanceof RelationField)//
				.map(f -> (RelationField) f)//
				.collect(Collectors.toSet());
		writer.write(SECTION_DELIMITER + LINE_DELIMITER);
		writeSections(writer, this.relationMap.values(), relationFields);
	}

	private <F extends ISectionField, S extends AbstractSection<F>> void writeSections(Writer writer,
			Collection<S> sections, Collection<F> fields) throws IOException {
		boolean firstSection = true;
		for (AbstractSection<F> section : sections) {
			if (!firstSection) {
				writer.write(SECTION_DELIMITER + LINE_DELIMITER);
			} else {
				firstSection = false;
			}
			section.write(writer, fields);
		}
	}

	/**
	 * 指定された {@link A5ER} の、指定されたManagerフィールドの値をマージする。
	 * 
	 * @param anotherEr マージ元の {@link A5ER}
	 */
	public void mergeManager(A5ER anotherEr) {
		ArgValidateUtil.notNull(anotherEr);

		Collection<ManagerField> anotherFields = anotherEr.managerSection.getFields();
		anotherFields.stream()//
				.forEach(af -> {
					anotherEr.managerSection.getValue(af)//
							.ifPresent(av -> this.managerSection.setValue(af, av.toValue()));
					;
				});
	}

	/**
	 * 指定された {@link A5ER} の、指定されたEntityフィールドの値をマージする。
	 * 
	 * @param anotherEr マージ元の {@link A5ER}
	 */
	public void mergeEntity(A5ER anotherEr) {
		ArgValidateUtil.notNull(anotherEr);

		anotherEr.entityMap.entrySet().stream()//
				.forEach(e -> merge(e.getKey(), e.getValue(), this.entityMap));
	}

	/**
	 * 指定された {@link A5ER} の、指定されたRelationフィールドの値をマージする。
	 * 
	 * @param anotherEr
	 */
	public void mergeRelation(A5ER anotherEr) {
		ArgValidateUtil.notNull(anotherEr);

		anotherEr.relationMap.entrySet().stream()//
				.forEach(e -> merge(e.getKey(), e.getValue(), this.relationMap));
	}

	private <F extends ISectionField, K extends ISectionKey<F>, S extends AbstractSection<F>> void merge(K anotherKey,
			S anotherSection, Map<K, S> sectionMap) {
		S thisSection = sectionMap.get(anotherKey);
		if (thisSection == null) {
			return;
		}
		Collection<F> anotherFields = anotherSection.getFields();
		anotherFields.forEach(af -> {
			anotherSection.getValue(af).ifPresent(av -> thisSection.setValue(af, av.toValue()));
		});
	}

	void addHeader(String header) {
		this.header.add(header);
	}

	void setManagerSection(ManagerSection section) {
		this.managerSection = section;
	}

	void addEntity(EntitySectionKey key, EntitySection section) {
		this.entityMap.put(key, section);
	}

	void addRelation(RelationSectionKey key, RelationSection section) {
		this.relationMap.put(key, section);
	}

	/**
	 * Managerセクションのフィールドの値を取得する。
	 * 
	 * @param field
	 * @return
	 */
	public Optional<IValue> getValue(ManagerField field) {
		ArgValidateUtil.notNull(field);

		return this.managerSection.getValue(field);
	}

	public void setValue(ManagerField field, String value) {
		ArgValidateUtil.notNull(field, value);

		managerSection.setValue(field, value);
	}

	/**
	 * Entityセクションのフィールドの値を取得する。
	 * 
	 * @param key
	 * @param field
	 * @return
	 */
	public List<IValue> getValues(EntitySectionKey key, EntityField field) {
		ArgValidateUtil.notNull(key, field);

		EntitySection section = this.entityMap.get(key);
		if (section == null) {
			return Collections.emptyList();
		}

		return section.getValues(field);
	}

	/**
	 * Relationセクションのフィールドの値を取得する。
	 * 
	 * @param key
	 * @param field
	 * @return
	 */
	public List<IValue> getValues(RelationSectionKey key, RelationField field) {
		ArgValidateUtil.notNull(key, field);

		RelationSection section = this.relationMap.get(key);
		if (section == null) {
			return Collections.emptyList();
		}

		return section.getValues(field);
	}
}
