package com.example.a5er.section;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import lombok.EqualsAndHashCode;

/**
 * Managerセクション
 * 
 * @author covar
 */
public class ManagerSection extends AbstractSection<ManagerField> {

	@EqualsAndHashCode
	public static class ManagerSectionKey implements ISectionKey<ManagerField> {
		private static final Set<ManagerField> KEY_FIELDS = Collections.unmodifiableSet(new HashSet<>());

		private Map<ManagerField, String> fieldValueMap = new HashMap<>();

		private ManagerSectionKey(ManagerSection section) {
			KEY_FIELDS.forEach(f -> {
				section.getValue(f).ifPresent(v -> this.fieldValueMap.put(f, v.toValue()));
			});
		}

		public static ManagerSectionKey generateKey(ManagerSection section) {
			return new ManagerSectionKey(section);
		}

		@Override
		public boolean isKeyField(ManagerField field) {
			return KEY_FIELDS.stream().anyMatch(f -> f == field);
		}
	}

	@Override
	public ManagerSectionKey generateKey() {
		return ManagerSectionKey.generateKey(this);
	}

	@Override
	public SectionType sectionType() {
		return SectionType.MANAGER;
	}

	@Override
	protected Optional<ManagerField> getField(String fieldName) {
		return ManagerField.enumOf(fieldName);
	}

}
