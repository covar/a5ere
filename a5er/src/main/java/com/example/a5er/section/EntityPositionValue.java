package com.example.a5er.section;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * EntityセクションのPositionフィールドの値クラス
 * 
 * @author covar
 */
@Accessors(chain = true)
@Data
public class EntityPositionValue implements IValue {

	private static final String VALUE_DELIMITER = ",";

	private String page;

	private String x;

	private String y;

	private String w;

	private String h;

	public EntityPositionValue(String value) {
		String[] values = value.split(VALUE_DELIMITER);
		page = values[0];
		x = values[1];
		y = values[2];
		if (4 <= values.length) {
			w = values[3];
		}
		if (5 <= values.length) {
			h = values[4];
		}
	}

	@Override
	public String toValue() {
		StringBuilder sb = new StringBuilder()//
				.append(page)//
				.append(VALUE_DELIMITER).append(x)//
				.append(VALUE_DELIMITER).append(y)//
		;

		if (StringUtils.isNotEmpty(w)) {
			sb.append(VALUE_DELIMITER).append(w);
		}

		if (StringUtils.isNotEmpty(h)) {
			sb.append(VALUE_DELIMITER).append(h);
		}

		return sb.toString();
	}

	@Override
	public boolean eqPage(String value) {
		EntityPositionValue newValue = new EntityPositionValue(value);
		return this.page.equalsIgnoreCase(newValue.page);
	}

	@Override
	public IValue create(String value) {
		return new EntityPositionValue(value);
	}
}
