package com.example.a5er.section;

public class ManagerPageValue implements IValue {

	private String page;

	public ManagerPageValue(String value) {
		page = value;
	}

	@Override
	public String toValue() {
		return page;
	}

	@Override
	public boolean eqPage(String value) {
		ManagerPageValue newValue = new ManagerPageValue(value);
		return page.equalsIgnoreCase(newValue.page);
	}

	@Override
	public IValue create(String value) {
		return new ManagerPageValue(value);
	}

}
