package com.example.a5er.section;

/**
 * フィールド定義
 * 
 * @author covar
 */
public interface ISectionField {

	String getFieldName();

	SectionType getSectionType();

	IValue createValue(String value);
}
