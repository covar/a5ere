package com.example.a5er.section;

/**
 * セクションのフィールドの文字列値クラス
 * 
 * @author covar
 */
public class StringValue implements IValue {

	private String value;

	public StringValue(String value) {
		this.value = value;
	}

	@Override
	public String toValue() {
		return value;
	}

	@Override
	public boolean eqPage(String value) {
		return true;
	}

	@Override
	public IValue create(String value) {
		return new StringValue(value);
	}

}
