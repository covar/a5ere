package com.example.a5er.section;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

import lombok.EqualsAndHashCode;

/**
 * Relationセクション
 * 
 * @author covar
 */
public class RelationSection extends AbstractSection<RelationField> {

	@EqualsAndHashCode
	public static class RelationSectionKey implements ISectionKey<RelationField> {

		private static final Collection<RelationField> KEY_FIELDS = Collections
				.unmodifiableSet(new HashSet<>(Arrays.asList(RelationField.ENTITY_1, RelationField.ENTITY_2,
						RelationField.FIELDS_1, RelationField.FIELDS_2)));

		private Map<RelationField, String> fieldValueMap = new HashMap<>();

		private RelationSectionKey(RelationSection section) {
			KEY_FIELDS.forEach(f -> {
				section.getValue(f).ifPresent(v -> this.fieldValueMap.put(f, v.toValue()));
			});
		}

		public static RelationSectionKey generateKey(RelationSection section) {
			return new RelationSectionKey(section);
		}

		@Override
		public boolean isKeyField(RelationField field) {
			return KEY_FIELDS.stream().anyMatch(f -> f == field);
		}
	}

	@Override
	public RelationSectionKey generateKey() {
		return RelationSectionKey.generateKey(this);
	}

	@Override
	public SectionType sectionType() {
		return SectionType.RELATION;
	}

	@Override
	protected Optional<RelationField> getField(String fieldName) {
		return RelationField.enumOf(fieldName);
	}
}
