package com.example.a5er.section;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import lombok.EqualsAndHashCode;

/**
 * Entityセクション
 * 
 * @author covar
 */
public class EntitySection extends AbstractSection<EntityField> {

	@EqualsAndHashCode
	public static class EntitySectionKey implements ISectionKey<EntityField> {
		private static final Set<EntityField> KEY_FIELDS = Collections
				.unmodifiableSet(new HashSet<>(Arrays.asList(EntityField.P_NAME)));

		private Map<EntityField, String> fieldValueMap = new HashMap<>();

		private EntitySectionKey(EntitySection section) {
			KEY_FIELDS.forEach(f -> {
				section.getValue(f).ifPresent(v -> this.fieldValueMap.put(f, v.toValue()));
			});
		}

		public static EntitySectionKey generateKey(EntitySection section) {
			return new EntitySectionKey(section);
		}

		@Override
		public boolean isKeyField(EntityField field) {
			return KEY_FIELDS.stream().anyMatch(f -> f == field);
		}
	}

	@Override
	public EntitySectionKey generateKey() {
		return EntitySectionKey.generateKey(this);
	}

	@Override
	public SectionType sectionType() {
		return SectionType.ENTITY;
	}

	@Override
	protected Optional<EntityField> getField(String fieldName) {
		return EntityField.enumOf(fieldName);
	}
}
