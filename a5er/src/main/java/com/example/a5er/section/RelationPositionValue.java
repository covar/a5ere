package com.example.a5er.section;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * RelationセクションのPositionフィールドの値クラス
 * 
 * @author covar
 */
@Accessors(chain = true)
@Data
public class RelationPositionValue implements IValue {

	private static final String VALUE_DELIMITER = ",";

	private String page;

	private String a;

	private String b;

	private String c;

	private String d;

	private String d1;

	private String d2;

	public RelationPositionValue(String value) {
		String[] values = value.split(VALUE_DELIMITER);
		page = values[0];
		a = values[1];
		b = values[2];
		c = values[3];
		d = values[4];
		d1 = values[5];
		d2 = values[6];
	}

	@Override
	public String toValue() {
		return new StringBuilder()//
				.append(page)//
				.append(VALUE_DELIMITER).append(a)//
				.append(VALUE_DELIMITER).append(b)//
				.append(VALUE_DELIMITER).append(c)//
				.append(VALUE_DELIMITER).append(d)//
				.append(VALUE_DELIMITER).append(d1)//
				.append(VALUE_DELIMITER).append(d2)//
				.toString();
	}

	@Override
	public boolean eqPage(String value) {
		RelationPositionValue newValue = new RelationPositionValue(value);
		return this.page.equalsIgnoreCase(newValue.page);
	}

	@Override
	public IValue create(String value) {
		return new RelationPositionValue(value);
	}
}
