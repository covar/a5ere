package com.example.a5er.section;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;

import lombok.Getter;

/**
 * フィールド定義
 * 
 * @author covar
 */
public enum ManagerField implements ISectionField {

	PROJECT_NAME("ProjectName", StringValue::new), //
	AUTHOR("Author", StringValue::new), //
	MAX_ENTITY_ROW_SHOW("MaxEntityRowShow", StringValue::new), //
	READ_ONLY_RECOMMEND("ReadOnlyRecommend", StringValue::new), //
	PAGE("Page", ManagerPageValue::new), //
	PAGE_INFO("PageInfo", ManagerPageInfoValue::new), //
	LOGICAL_VIEW("LogicalView", StringValue::new), //
	DECODE_DOMAIN("DecodeDomain", StringValue::new), //
	VIEW_MODE_PAGE_INDIVIDUALLY("ViewModePageIndividually", StringValue::new), //
	VIEW_MODE("ViewMode", StringValue::new), //
	VIEW_FORMAT("ViewFormat", StringValue::new), //
	USE_NONDEPENDENCE_DASH_LINE("UseNondependenceDashLine", StringValue::new), //
	FONT_NAME("FontName", StringValue::new), //
	FONT_SIZE("FontSize", StringValue::new), //
	PAPER_SIZE("PaperSize", StringValue::new), //
	HEADER_LEFT("HeaderLeft", StringValue::new), //
	HEADER_CENTER("HeaderCenter", StringValue::new), //
	HEADER_RIGHT("HeaderRight", StringValue::new), //
	FOOTER_LEFT("FooterLeft", StringValue::new), //
	FOOTER_CENTER("FooterCenter", StringValue::new), //
	FOOTER_RIGHT("FooterRight", StringValue::new), //
	SHOW_PAGEOUT_RELATION("ShowPageoutRelation", StringValue::new), //
	DEFAULT_PK_NAME("DefaultPkName", StringValue::new), //
	DEFAULT_PK_INDEX_NAME("DefaultPkIndexName", StringValue::new), //
	DEFAULT_INDEX_NAME("DefaultIndexName", StringValue::new), //
	DEFAULT_FK_NAME("DefaultFkName", StringValue::new), //
	SQL_SEPARATOR("SqlSeparator", StringValue::new), //
	SHOW_TAG("ShowTag", StringValue::new), //
	SHOW_COMMON_ATTRIBUTES("ShowCommonAttributes", StringValue::new),//
	;

	@Getter
	private String fieldName;

	private Function<String, IValue> valueMapper;

	private ManagerField(String fieldName, Function<String, IValue> valueMapper) {
		this.fieldName = fieldName;
		this.valueMapper = valueMapper;
	}

	@Override
	public SectionType getSectionType() {
		return SectionType.MANAGER;
	}

	public static Optional<ManagerField> enumOf(String fieldName) {
		return Arrays.stream(ManagerField.values())//
				.filter(f -> f.getFieldName().equals(fieldName))//
				.findFirst();
	}

	@Override
	public IValue createValue(String value) {
		return valueMapper.apply(value);
	}
}
