package com.example.a5er.section;

public class ManagerPageInfoValue implements IValue {

	private static final String FIELD_DELIMITER = ",";

	private String page;

	private String level;

	private String size;

	private String color;

	public ManagerPageInfoValue(String value) {
		String[] values = value.split(FIELD_DELIMITER);
		page = values[0];
		level = values[1];
		size = values[2];
		color = values[3];
	}

	@Override
	public String toValue() {
		return new StringBuilder()//
				.append(page)//
				.append(FIELD_DELIMITER).append(level)//
				.append(FIELD_DELIMITER).append(size)//
				.append(FIELD_DELIMITER).append(color)//
				.toString();
	}

	@Override
	public boolean eqPage(String value) {
		ManagerPageInfoValue newValue = new ManagerPageInfoValue(value);
		return this.page.equalsIgnoreCase(newValue.page);
	}

	@Override
	public IValue create(String value) {
		return new ManagerPageInfoValue(value);
	}

}
