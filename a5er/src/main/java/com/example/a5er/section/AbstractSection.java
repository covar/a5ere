package com.example.a5er.section;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.example.a5er.A5ER;
import com.example.a5er.util.ArgValidateUtil;

/**
 * セクション
 * 
 * @author covar
 */
public abstract class AbstractSection<FIELD extends ISectionField> {

	private Map<FIELD, List<IValue>> fieldValuesMap = new LinkedHashMap<>();

	/**
	 * {@link SectionType}を取得する。
	 * 
	 * @return
	 */
	abstract public SectionType sectionType();

	/**
	 * セクションのキー情報を生成する。
	 * 
	 * @return
	 */
	abstract public ISectionKey<FIELD> generateKey();

	/**
	 * セクションのフィールドを取得する。
	 * 
	 * @return
	 */
	abstract protected Optional<FIELD> getField(String fieldName);

	public static interface ISectionKey<FIELD> {
		boolean isKeyField(FIELD field);
	}

	public void addField(FIELD field, String value) {
		ArgValidateUtil.notNull(field, value);

		if (!fieldValuesMap.containsKey(field)) {
			fieldValuesMap.put(field, new ArrayList<>());
		}
		IValue v = field.createValue(value);
		fieldValuesMap.get(field).add(v);
	}

	public void addField(String fieldName, String value) {
		getField(fieldName).ifPresent(f -> addField(f, value));
	}

	public Collection<FIELD> getFields() {
		return fieldValuesMap.keySet().stream()//
				.collect(Collectors.toSet());
	}

	/**
	 * フィールドの値を取得する。</br>
	 * 同名のフィールドが複数存在する場合があるため、結果は {@link List}
	 * 
	 * @param field
	 * @return
	 */
	public List<IValue> getValues(FIELD field) {
		ArgValidateUtil.notNull(field);

		List<IValue> values = fieldValuesMap.get(field);
		if (values == null) {
			return Collections.emptyList();
		}
		return values;
	}

	/**
	 * フィールドの値を取得する。</br>
	 * 同名のフィールドが複数存在する場合は先頭のフィールドの値を取得する。
	 * 
	 * @param field
	 * @return
	 */
	public Optional<IValue> getValue(FIELD field) {
		ArgValidateUtil.notNull(field);

		List<IValue> values = getValues(field);
		if (CollectionUtils.isEmpty(values)) {
			return Optional.empty();
		}
		return Optional.of(values.get(0));
	}

	/**
	 * フィールドに値を設定する。</br>
	 * 同名のフィールドが複数存在する場合は先頭のフィールドに値を設定する。
	 * 
	 * @param field
	 * @param value
	 */
	public void setValue(FIELD field, String value) {
		ArgValidateUtil.notNull(field, value);

		List<IValue> values = getValues(field);
		List<IValue> resultValues = values.stream()//
				.map(v -> {
					if (v.eqPage(value)) {
						return v.create(value);
					}
					return v;
				})//
				.collect(Collectors.toList());
		this.fieldValuesMap.put(field, resultValues);
	}

	/**
	 * セクションを書き出す。
	 * 
	 * @param write
	 * @throws IOException
	 */
	public void write(Writer writer, Collection<FIELD> targetFields) throws IOException {
		ArgValidateUtil.notNull(writer, targetFields);

		SectionType sectionType = sectionType();
		writer.write(sectionType.getSectionName() + A5ER.LINE_DELIMITER);

		ISectionKey<FIELD> key = generateKey();

		for (Entry<FIELD, List<IValue>> entry : fieldValuesMap.entrySet()) {
			FIELD field = entry.getKey();

			if (!key.isKeyField(field) && !targetFields.contains(field)) {
				continue;
			}

			List<IValue> values = entry.getValue();
			for (IValue value : values) {
				writer.write(field.getFieldName() + A5ER.FIELD_VALUE_DELIMITER + value.toValue() + A5ER.LINE_DELIMITER);
			}
		}
	}
}
