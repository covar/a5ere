package com.example.a5er.section;

import java.util.Arrays;
import java.util.Optional;

import lombok.Getter;

/**
 * セクション定義
 * 
 * @author covar
 */
public enum SectionType {
	MANAGER("[Manager]"), //
	ENTITY("[Entity]"), //
	RELATION("[Relation]"),//
	;

	@Getter
	private String sectionName;

	private SectionType(String sectionName) {
		this.sectionName = sectionName;
	}

	public static Optional<SectionType> enumOf(String sectionName) {
		return Arrays.stream(SectionType.values())//
				.filter(s -> s.sectionName.equals(sectionName))//
				.findFirst();
	}
}
