package com.example.a5er.section;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;

/**
 * フィールド定義
 * 
 * @author covar
 */
public enum EntityField implements ISectionField {

	P_NAME("PName", StringValue::new), //
	L_NAME("LName", StringValue::new), //
	COMMENT("Comment", StringValue::new), //
	TABLE_OPTION("TableOption", StringValue::new), //
	PAGE("Page", StringValue::new), //
	LEFT("Left", StringValue::new), //
	TOP("Top", StringValue::new), //
	FIELD("Field", StringValue::new), //
	EFFECT_MODE("EffectMode", StringValue::new), //
	COLOR("Color", StringValue::new), //
	BK_COLOR("BkColor", StringValue::new), //
	POSITION("Position", EntityPositionValue::new), //
	Z_ORDER("ZOrder", StringValue::new),//
	;

	private String fieldName;

	private Function<String, IValue> valueMapper;

	private EntityField(String fieldName, Function<String, IValue> valueMapper) {
		this.fieldName = fieldName;
		this.valueMapper = valueMapper;
	}

	@Override
	public SectionType getSectionType() {
		return SectionType.ENTITY;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	public static Optional<EntityField> enumOf(String fieldName) {
		return Arrays.stream(EntityField.values())//
				.filter(f -> f.getFieldName().equals(fieldName))//
				.findFirst();
	}

	@Override
	public IValue createValue(String value) {
		return valueMapper.apply(value);
	}
}
