package com.example.a5er.section;

/**
 * セクションのフィールドの値インターフェース
 * 
 * @author covar
 */
public interface IValue {

	String toValue();

	boolean eqPage(String value);

	IValue create(String value);
}
