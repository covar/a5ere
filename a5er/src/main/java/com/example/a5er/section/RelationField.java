package com.example.a5er.section;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;

/**
 * フィールド定義
 * 
 * @author covar
 */
public enum RelationField implements ISectionField {

	ENTITY_1("Entity1", StringValue::new), //
	ENTITY_2("Entity2", StringValue::new), //
	RELATION_TYPE_1("RelationType1", StringValue::new), //
	RELATION_TYPE_2("RelationType2", StringValue::new), //
	FIELDS_1("Fields1", StringValue::new), //
	FIELDS_2("Fields2", StringValue::new), //
	CARDINARITY_1("Cardinarity1", StringValue::new), //
	CARDINARITY_2("Cardinarity2", StringValue::new), //
	POSITION("Position", RelationPositionValue::new), //
	DEPENDENCE("Dependence", StringValue::new), //
	CAPTION("Caption", StringValue::new), //
	P_NAME("PName", StringValue::new), //
	LINE_MODE("LineMode", StringValue::new), //
	BAR_1("Bar1", StringValue::new), //
	BAR_2("Bar2", StringValue::new), //
	BAR_3("Bar3", StringValue::new), //
	TERMPOS_1("TermPos1", StringValue::new), //
	TERMPOS_2("TermPos2", StringValue::new), //
	Z_ORDER("ZOrder", StringValue::new),//
	;

	private String fieldName;

	private Function<String, IValue> valueMapper;

	private RelationField(String fieldName, Function<String, IValue> valueMapper) {
		this.fieldName = fieldName;
		this.valueMapper = valueMapper;
	}

	@Override
	public SectionType getSectionType() {
		return SectionType.RELATION;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	public static Optional<RelationField> enumOf(String fieldName) {
		return Arrays.stream(RelationField.values())//
				.filter(f -> f.getFieldName().equals(fieldName))//
				.findFirst();
	}

	@Override
	public IValue createValue(String value) {
		return valueMapper.apply(value);
	}
}
