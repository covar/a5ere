package com.example.a5er.util;

import java.util.Arrays;

/**
 * 引数バリデーションユーティリティ
 * 
 * @author covar
 */
public class ArgValidateUtil {

	private ArgValidateUtil() {
	}

	public static void notNull(Object... args) {
		IllegalArgumentException e = new IllegalArgumentException();
		if (args == null) {
			throw e;
		}

		boolean isInvalid = Arrays.stream(args)//
				.anyMatch(a -> a == null);
		if (isInvalid) {
			throw e;
		}
	}
}
